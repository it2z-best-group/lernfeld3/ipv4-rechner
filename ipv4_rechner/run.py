import typer
from rich import print
import fastapi
from ipv4_rechner.helper import Mode, convert, parse, get_sum_clients, cidr
import uvicorn

app = typer.Typer()
api = fastapi.FastAPI()


@app.command(
    help="calculate the net-, broadcast-address and sum of possible clients in a given network"
)
def calc(ip: str, mask: str) -> None:
    """
    calculate the net-, broadcast-address and sum of possible clients in a given network

    :param ip: an ipv4 address in the network
    :param mask: the networks mask
    """
    print("Netzadresse: ", convert(ip := parse(ip), mask := parse(mask), Mode.NET))
    print("Broadcastadresse: ", convert(ip, mask, Mode.BROAD))
    print("Sum Clients: ", get_sum_clients(mask))


@app.command()
def v2(cidr_notation: str, subn: list[int]):
    a = cidr(cidr_notation, subn)
    print(a)


@api.get("/old")
async def api_old(ip: str, mask: str):
    return {
        "netzadresse": convert(ip := parse(ip), mask := parse(mask), Mode.NET),
        "broadcastadresse": convert(ip, mask, Mode.BROAD),
        "sum_clients": get_sum_clients(mask),
    }

@api.post("/")
async def api_root(cidr_notation: str, subns: list[int]):
    return cidr(cidr_notation, subns)


@app.command(help="start the api server")
def serve(port: int = 8000):
    uvicorn.run("ipv4_rechner.run:api", host="0.0.0.0", port=port)


if __name__ == "__main__":
    app()
