from enum import Enum
from typing import TypedDict


class Mode(Enum):
    """
    Enum with either `NET` or `BROAD` mode for `convert()`
    """

    NET = 0
    BROAD = 1


class Subns(TypedDict):
    subn: int
    size: int
    mask: str
    net: str
    broad: str


def parse(ip: str) -> str:
    """
    parse an ipv4 address and return a 32 character long string of all 8-bit parts converted to binary and concatinated

    :param ip: any valid ipv4 address
    """
    return "".join([format(int(y), "#010b")[2:] for y in ip.split(".")])


def convert(parsed_ip: str, parsed_mask: str, mode: Mode) -> str:
    """
    converts a 32 character long binary string to either a net or broadcast ip based on `Mode`

    :param parsed_ip: an ip that was previously parsed by `parse()`
    :param parsed_mask: a mask that was previously parsed by `parse()`
    :param mode: either Mode.NET or Mode.BROAD
    """
    ip = list(parsed_ip)
    for i, c in enumerate(parsed_mask[::-1]):
        if c != "0":
            break
        ip[-(i + 1)] = str(mode.value)

    addr = ""
    for i, c in enumerate(ip):
        if not i % 8 and i > 0:
            addr += "."
        addr += c

    return ".".join([str(int(x, 2)) for x in addr.split(".")])


def get_sum_clients(parsed_mask: str) -> int:
    """
    get the sum of possible clients in a given mask

    :param parsed_mask: a mask that was previously parsed by `parse()`
    """
    i = 0
    for c in parsed_mask[::-1]:
        if c != "0":
            break
        i += 1

    return (2**i) - 2


def generate_sizes():
    """
    yields infinitely many `2**n`. supposed to stop once you find a target where n can fit.
    """
    i = 0
    while True:
        yield 2**i
        i += 1


def cidr(cidr_notation: str, wanted_subnets: list[int]) -> list[Subns]:
    """
    handle cidr notation. this function is cursed and needs more work but time is running low. im not proud of whats happening here

    :param cidr_notation: the initial ip/mask in cidr notation
    :param wanted_subnets: a list of the sizes of the wanted subnets
    """
    ip, mask = cidr_notation.split("/")
    ip = parse(ip)

    subns: list[Subns] = []

    for subn in sorted(wanted_subnets, reverse=True):
        _t = 0
        for size in generate_sizes():
            if size > subn:  # ? subn+2
                break
            _t += 1
        mask = "1" * (32 - _t) + "0" * _t
        net = convert(ip, mask, Mode.NET)
        broad = convert(ip, mask, Mode.BROAD)
        m = ""
        for i, c in enumerate(mask):
            if not i % 8 and i > 0:
                m += "."
            m += c
        subns.append(
            {
                "subn": subn,
                "size": size,
                "mask": ".".join([str(int(x, 2)) for x in m.split(".")]),
                "net": net,
                "broad": broad,
            }
        )

        # TODO: this needs work but its a hacky way of dealing with the last three octets for now...
        splitted_broad = broad.split(".")

        if splitted_broad[-1] == "255":
            splitted_broad[-1] = "0"
            if splitted_broad[-2] == "255":
                splitted_broad[-2] = "0"
                if splitted_broad[-3] == "255":
                    splitted_broad[-3] = "0"
                else:
                    splitted_broad[-3] = str(int(splitted_broad[-3]) + 1)
            else:
                splitted_broad[-2] = str(int(splitted_broad[-2]) + 1)
        else:
            splitted_broad[-1] = str(int(splitted_broad[-1]) + 1)

        ip = parse(".".join(splitted_broad))

    return subns
