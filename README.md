# [IPv4] Rechner

[Lernfeld 03] IT - [Client]s in [Netzwerk]e einbinden

## Aufgabenstellung

Programmieren Sie einen [IPv4]-[Subnetz]rechner. Dieser soll zu einer [IP-Adresse] mit [Subnetzmaske], die [NetzID] und sie Anzahl der möglichen [Client]s berechnen.  
Erweiterungen ([VLSM], [Supernetting]) sind gerne gesehen. Seien Sie kreativ!

## Dokumentation

Die [docs] sind generiert mit [Sphinx] und veröffentlicht über [GitLab Pages]

[subnetz]: https://de.wikipedia.org/wiki/Subnetz
[ipv4]: https://de.wikipedia.org/wiki/IPv4
[ip-adresse]: https://de.wikipedia.org/wiki/IP-Adresse
[netzwerk]: https://de.wikipedia.org/wiki/Rechnernetz
[subnetzmaske]: https://de.wikipedia.org/wiki/Netzmaske
[netzid]: https://de.wikipedia.org/wiki/Classless_Inter-Domain_Routing#Berechnung
[client]: https://de.wikipedia.org/wiki/Client
[vlsm]: https://de.wikipedia.org/wiki/Variable_Length_Subnet_Mask "Variable Length Subnet Mask"
[supernetting]: https://de.wikipedia.org/wiki/Supernetting
[lernfeld 03]: https://moodle.itech-bs14.de/mod/page/view.php?id=102234
[docs]: https://it2z-best-group.gitlab.io/lernfeld3/ipv4-rechner
[gitlab pages]: https://docs.gitlab.com/ee/user/project/pages/
[sphinx]: https://www.sphinx-doc.org/
