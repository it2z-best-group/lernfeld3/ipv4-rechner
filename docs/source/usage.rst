Usage 
=====

Calculate
---------

.. _execution:

CLI
---

.. code-block:: console

    $ ipv4-rechner calc 192.168.178.20 255.255.255.0 

    Netzadresse:  192.168.178.0
    Broadcastadresse:  192.168.178.255
    Sum Clients:  254

or get help with

.. code-block:: console

    $ ipv4-rechner calc --help                      

    Usage: ipv4-rechner calc [OPTIONS] IP MASK

    calculate the net-, broadcast-address and sum of possible clients in a given network

    ╭─ Arguments ──────────────────────────────────────────────────────────────────────────────╮
    │ *    ip        TEXT  [default: None] [required]                                          │
    │ *    mask      TEXT  [default: None] [required]                                          │
    ╰──────────────────────────────────────────────────────────────────────────────────────────╯
    ╭─ Options ────────────────────────────────────────────────────────────────────────────────╮
    │ --help          Show this message and exit.                                              │
    ╰──────────────────────────────────────────────────────────────────────────────────────────╯

v2 can calculate vlsm by passing ip/mask and a list of wanted sizes:

.. code-block:: console

    $ ipv4-rechner v2 192.169.0.0/16 20 50 130 5000 69

    [
        {'subn': 5000, 'size': 8192, 'mask': '255.255.224.0', 'net': '192.168.0.0', 'broad': '192.168.31.255'},
        {'subn': 130, 'size': 256, 'mask': '255.255.255.0', 'net': '192.168.32.0', 'broad': '192.168.32.255'},
        {'subn': 69, 'size': 128, 'mask': '255.255.255.128', 'net': '192.168.33.0', 'broad': '192.168.33.127'},
        {'subn': 50, 'size': 64, 'mask': '255.255.255.192', 'net': '192.168.33.128', 'broad': '192.168.33.191'},
        {'subn': 20, 'size': 32, 'mask': '255.255.255.224', 'net': '192.168.33.192', 'broad': '192.168.33.223'}
    ]

API 
---

.. code-block:: console

    $ ipv4-rechner serve

    INFO:     Started server process [69]
    INFO:     Waiting for application startup.
    INFO:     Application startup complete.
    INFO:     Uvicorn running on http://0.0.0.0:8000 (Press CTRL+C to quit)

for a get-request to `/old` with query parameters `?ip=192.168.178.20&mask=255.255.255.0` the api returns a json object. An example curl request to this api may look like:

.. code-block:: console

    $ curl "http://localhost:8000/old?ip=192.168.178.20&mask=255.255.255.0"
    
    {"netzadresse":"192.168.178.0","broadcastadresse":"192.168.178.255","sum_clients":254}
    