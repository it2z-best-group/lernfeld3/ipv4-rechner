Functions
=========

run
---

.. autofunction:: ipv4_rechner.run.calc

Check out the :doc:`usage` section for further information, including how to
:ref:`execute <execution>` the project.

helper
------

.. autofunction:: ipv4_rechner.helper.parse

.. autofunction:: ipv4_rechner.helper.convert

.. autofunction:: ipv4_rechner.helper.get_sum_clients

.. autofunction:: ipv4_rechner.helper.generate_sizes

.. autofunction:: ipv4_rechner.helper.cidr

.. autoclass:: ipv4_rechner.helper.Mode